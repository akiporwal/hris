# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Trackon Resource Project Management System. A dashboard application to track employee carrer path within the organisation.

* Version

### How do I get set up? ###

* Summary of set up

1. Install Visual studio 2012, MS SQL 2012, Node.js, Ruby installer, git, fiddler.

* Configuration
* Dependencies
* Database configuration

1. Run dbscript project files in ms sql server.
* How to run tests

1. grunt test

* Deployment instructions

1. Select HrisTool and publish to local file system which is front end web application
2. Select HrisToolWebApi and publish to local file system which is api v2 application

### Contribution guidelines ###
* Before making changes in this repo

1. Create new branch on which task or userstory you were working that needs to push to server.
2. Also make a local temp branch instead of directly working on replica of server branch.
3. Before push your local changes to replica of server branch make sure you commits are on top of others commits.
4. **SHOULD NOT PUSH ANY MERGE COMMITS OR INTERMEDIATE COMMITS INTO THIS REPO.**

* Writing tests
* **Code review**

* Min 1 approve is need to merge to upword branch

* Other guidelines

### Who do I talk to? ###

* Repo owner