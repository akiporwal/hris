﻿var app = angular.module('HrisApp', []);
app.controller('DetailsCtrl', function ($scope) {
    $scope.searchItems = [
        ".Net",
        "Big Data",
        "Business Analysis",
        "Business Intelligence",
        "C++",
        "COBAL",
        "Cold fusion",
        "Communication",
        "CRM",
        "Database",
        "Dataware Housing",
        "Domain",
        "Enbedded Domain Technology",
        "ERP",
        "Finance",
        "HR",
        "IT/Information Security",
        "Java",
        "JavaScript",
        "L&D",
        "Legal",
        "Linux",
        "Logistics",
        "MainFrames Tools/utils",
        "Microsoft Technologies",
        "Middleware",
        "Mobile technology",
        "Networking",
        "Operating Systems",
        "Other tools for Cobol",
        "PHP",
        "Project Management",
        "Quality and process management",
        "Technical review",
        "Testing",
        "UI Designer",
        "Virtualization",
        "Web Dvelopment",
        "Windows"
    ];


    $scope.searchItems.sort();

    $scope.suggestions1 = [];
    $scope.suggestions2 = [];
    $scope.PrimarySkills = [];
    $scope.SecondarySkills = [];



    $scope.searchPrimarySkill = function (Primary) {
        $scope.suggestions1 = [];
        var myMaxSuggestionListLength1 = 0;
        for (var i = 0; i < $scope.searchItems.length; i++) {
            var searchItemsSmallLetters = angular.lowercase($scope.searchItems[i]);
            var searchTextSmallLetters = angular.lowercase($scope.PrimarySkill);
            if (searchItemsSmallLetters.indexOf(searchTextSmallLetters) !== -1) {
                $scope.suggestions1.push(searchItemsSmallLetters);
                if (Primary === "")
                    $scope.suggestions1 = [];
                myMaxSuggestionListLength1 += 1;
                if (myMaxSuggestionListLength1 == 5) {
                    break;
                }
            }
        }
    };
    $scope.searchSecondarySkill = function (Secondary) {
        $scope.suggestions2 = [];
        var myMaxSuggestionListLength2 = 0;
        for (var i = 0; i < $scope.searchItems.length; i++) {
            var searchItemsSmallLetters = angular.lowercase($scope.searchItems[i]);
            var searchTextSmallLetters = angular.lowercase($scope.SecondarySkill);
            if (searchItemsSmallLetters.indexOf(searchTextSmallLetters) !== -1) {
                $scope.suggestions2.push(searchItemsSmallLetters);
                if (Secondary === "")
                    $scope.suggestions2 = [];
                myMaxSuggestionListLength2 += 1;
                if (myMaxSuggestionListLength2 == 5) {
                    break;
                }
            }
        }
    };

    $scope.AddPrimarySkill = function (index) {
        var flag = 0;
        for (var j = 0; j <= $scope.PrimarySkills.length; j++) {
            if (angular.equals($scope.PrimarySkills[j], $scope.suggestions1[index])) {
                flag = 1;
                alert("Skill was already included");
            }
        }
        if (flag === 0) {
            $scope.PrimarySkills.push($scope.suggestions1[index]);

        }
        $scope.PrimarySkill = "";
        $scope.suggestions1 = [];

    };
    $scope.AddSecondarySkill = function (index) {

        var flag = 0;
        for (var j = 0; j <= $scope.SecondarySkills.length; j++) {
            if (angular.equals($scope.SecondarySkills[j], $scope.suggestions2[index])) {
                flag = 1;
                alert("Skill was already included");
            }
        }
        if (flag === 0) {
            $scope.SecondarySkills.push($scope.suggestions2[index]);
        }
        $scope.SecondarySkill = "";
        $scope.suggestions2 = [];

    };


    $scope.RemovePrimaryskill = function (x) {

        $scope.PrimarySkills.splice(x, 1);

    };

    $scope.RemoveSecondaryskill = function (x) {
        $scope.SecondarySkills.splice(x, 1);
    };


    $scope.hidepart = function () {
        if (Details.Band != B1 || Details.Band != B2 || Details.Band != B3 || Details.Band != A1 || Details.Band != A2 || Details.Band != A3)
            return false;
        else
            return true;
    };

    $scope.getExp = function (Experience) {
        var IntExperience = new Date(Experience);
        var today = new Date();
        var exp = Math.floor((today - IntExperience) / (2592000000));
        var expyear = Math.floor(exp / 12);
        var expmonth = (exp % 12);
        $scope.IntExp = expyear + " Years and " + expmonth + " months";
        var extexp = $scope.ExtExp;

        $scope.TotalExp = expyear + " Years " + expmonth + " months";
    };

    $scope.getExtExp = function (extyears, extmonths) {

        $scope.ExtExp = extyears + " Years, " + extmonths + "months";
    };

    $scope.getTotalExp = function (date, years, months) {
        var joinedDate = new Date(date);
        var todayis = new Date();
        var internalexp = Math.floor((todayis - joinedDate) / (2592000000));
        var month = years * 12;
        var allmonths = internalexp + month + months;
        var totexpyears = Math.floor(allmonths / 12);
        var totexpmonths = (allmonths % 12);
        $scope.TotalExp = totexpyears + " years," + totexpmonths + "months";
    };

});

function validations() {
    var first = document.getElementsByClassName('form-control');


    var i = first.length;
    for (var j = 0; j < i; j++) {

        if (first[j].value === "" && j !== 1 && j !== 11 && j !== 18 && j !== 19) {
            first[j].style = 'border-color:red';
            alert("Mandatory Fields cannot be empty");
            break;

        }
        if (first[j].value !== "")
            first[j].style = 'border-color:none';
    }
}




